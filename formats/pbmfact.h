#ifndef __CF_PBMFACT_H__
#define __CF_PBMFACT_H__

/*
    This file is part of Page Layout Detection Tools.

    This code is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This code is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <raster/bwfactory.h>

namespace pagetools{

class PBMFactory: public BWFactory{
public:
    PBMFactory(const char *path);
    virtual CntPtr<BWImage> create();
private:
    const char *path_;
};

}//namespace

#endif//__CF_PBMFACT_H__
