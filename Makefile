C++ = c++
WARNINGS =      -pedantic
C_FLAGS =       -MMD $(WARNINGS) -O2
C++_FLAGS =     -MMD $(WARNINGS) -O2

INCLUDES =      -I .
.SUFFIXES:      $(.SUFFIXES) .cpp
PTLIB = libptformats.a libptraster.a libptutil.a

LIBUTILOBJ = util/bitutil.o 
LIBRASTEROBJ = raster/bwimage.o raster/deskewer.o
LIBFORMATSOBJ = formats/pbmfact.o formats/tifffact.o

LIBPBM = pbm
# For Debian systems, uncomment this
LIBPBM = netpbm

# need to update this every time the version number changes!
VERSION = 0.1

%.o:%.c
	$(CC) $(C_FLAGS) $< $(INCLUDES) -c -o $@

%.o:%.cpp
	$(C++) $(C++_FLAGS) $< $(INCLUDES) -fno-implicit-templates -c -o $@

all: libptutil.a libptraster.a libptformats.a pbm_findskew/pbm_findskew tiff_findskew/tiff_findskew

libptutil.a:     $(LIBUTILOBJ)
	ar rus $@ $(LIBUTILOBJ)

libptraster.a:     $(LIBRASTEROBJ)
	ar rus $@ $(LIBRASTEROBJ)

libptformats.a:     $(LIBFORMATSOBJ)
	ar rus $@ $(LIBFORMATSOBJ)


pbm_findskew/pbm_findskew:  pbm_findskew/pbm_findskew.o $(PTLIB)
	$(C++) pbm_findskew/pbm_findskew.o $(PTLIB) -l$(LIBPBM) -o $@ $(LDFLAGS)

tiff_findskew/tiff_findskew:  tiff_findskew/tiff_findskew.o $(PTLIB)
	$(C++) tiff_findskew/tiff_findskew.o $(PTLIB) -ltiff -o $@ $(LDFLAGS)


clean:
	rm -rf */*.o core *.d */*.d $(PTLIB) pbm_findskew/pbm_findskew tiff_findskew/tiff_findskew

-include *.d

release:
	tar zcvf pagetools-$(VERSION).tar.gz COPYING Makefile TODO index.html *.txt */*.h */*.cpp */*.1
